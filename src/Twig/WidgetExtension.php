<?php
/**
 * Created by PhpStorm.
 * User: pix
 * Date: 22.03.17
 * Time: 20:51
 */

namespace Motvicka\WidgetBundle\Twig;


use ReflectionParameter;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WidgetExtension extends \Twig_Extension
{
	/**
	 * @var Container
	 */
	private $container;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction("widget", [$this, "widget"], [ "is_safe" => [ "html" ] ])
		];
	}

	public function widget($name, $arguments = [])
	{
		$pass = [];
		$widgetClass = $this->container->get("widget." . $name);
		$widgetClassReflection = new \ReflectionClass($widgetClass);
		$widgetFetchMethodReflection = $widgetClassReflection->getMethod("fetch");

		foreach($widgetFetchMethodReflection->getParameters() as $param) {
			/* @var $param ReflectionParameter */
			if (isset($arguments[$param->getName()])) {
				$pass[] = $arguments[$param->getName()];
			} else {
				$pass[] = $param->getDefaultValue();
			}
		}

		return $widgetFetchMethodReflection->invokeArgs($widgetClass, $pass);
	}

	public function getName()
	{
		return "widget";
	}

}