<?php
/**
 * Created by PhpStorm.
 * User: pix
 * Date: 22.03.17
 * Time: 21:11
 */

namespace Motvicka\WidgetBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class WidgetExtension extends Extension
{
	/**
	 * Load configuration.
	 */
	public function load(array $configs, ContainerBuilder $container)
	{
		$loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
		$loader->load("services.yml");
	}
}