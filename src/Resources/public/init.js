var pages = [];
var widgets = [];
var registeredWidgets = [];
var registeredPage;
var services = [];

$window = $(window);
$document = $(document);

function refreshWidgets() {
	$.map(registerWidgets, function (config) {
		if (widgets[config.name] != undefined) {
			registeredWidgets[config.id] = new widgets[config.name]($(config.node));
		} else {
			console.error("Missing '" + config.name + "' widget");
		}
	});
	registerWidgets = [];
}

function getParentWidget($object, name) {
	var $parentWidgetScriptObject;
	$object.parents().map(function () {
		var $this = $(this);
		var $script = $this.find("script[data-widget-name='" + name + "']");
		if ($script.length > 0 && $parentWidgetScriptObject == null) {
			$parentWidgetScriptObject = $script.first();
		}
	});

	if ($parentWidgetScriptObject) {
		return registeredWidgets[$parentWidgetScriptObject.attr("data-widget")];
	}
}

function getChildWidget($object, name) {
	var $script = $object.find("script[data-widget-name='" + name + "']");

	if ($script.length > 0) {
		return registeredWidgets[$script.attr("data-widget")];
	}
}

function getWidget(name) {
	var $script = $("script[data-widget-name='" + name + "']");

	if ($script.length > 0) {
		return registeredWidgets[$script.attr("data-widget")];
	}
}

function initPage() {
	var $body = $("body");
	var pageName = $body.attr("data-page");
	if (pageName && pages[pageName] != undefined) {
		registeredPage = new pages[pageName]($body);
	}
}

function getPage() {
	return registeredPage;
}

$(document).ready(function () {
	refreshWidgets();
	initPage();
});
