<?php
/**
 * Created by PhpStorm.
 * User: pix
 * Date: 22.03.17
 * Time: 20:57
 */

namespace Motvicka\WidgetBundle\Widget;


use Symfony\Bundle\TwigBundle\TwigEngine;

abstract class Widget
{
	/** @var TwigEngine */
	private $twigEngine;

	/**
	 * @return TwigEngine
	 */
	public function getTwigEngine()
	{
		return $this->twigEngine;
	}

	/**
	 * @param TwigEngine $twigEngine
	 */
	public function setTwigEngine($twigEngine)
	{
		$this->twigEngine = $twigEngine;
	}

	/**
	 * @param string $name
	 * @param string $body
	 * @return mixed
	 */
	public function generate($name, $body)
	{
		$ident = uniqid($name);

		$output = preg_replace(
			"~<(.*)>(.*)$~U",
			"<script type='text/javascript' data-widget='{$ident}' data-widget-name='{$name}'>registerWidget('{$name}', '{$ident}')</script><$1>$2",
			$body,
			1
		);

		return $output;
	}
}